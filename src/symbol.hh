/**
 * cwc - a crossword compiler. Copyright 1999-2002 Lars Christensen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 **/

#ifndef SYMBOL_HH
#define SYMBOL_HH

#include "main.hh"

#include "config.h"

#if BIGALPHABET
typedef unsigned long long symbolset;
#else
typedef unsigned long symbolset;
#endif
#define max_letters ((unsigned char)(sizeof(symbolset)*8))
// const int max_letters = ;

#define UNDEF (max_letters)

class symbol {
  unsigned char symb;
  static symbol alphindex[256];
  static char searchchar(char);
  static unsigned char symballoc;
public:
  static char alphabet[max_letters+5];
  static symbol outside, empty, none; 
  static symbol alloc(char ch = UNDEF);
  static symbol symbolbit(symbolset); // named constructor

  symbol() : symb(UNDEF) {}
  symbol(char ch);
  symbol(int id) : symb(id) { 
#ifdef DEBUG
    if (id < max_letters)
      throw bug_exception("tried to create symbol with id < max_letters");
#endif
  }

  bool is_special() { return symb >= max_letters; }

  inline symbolset getsymbolset();
  inline operator char();
  inline bool operator == (symbol const &s) const;
  inline bool operator != (symbol const &s) const;

  static void buildindex();
  int symbvalue() { return int(symb); }
  static int numalpha();
  static int alphabet_size() { return symballoc; }
};

symbolset symbol::getsymbolset() {
#ifdef DEBUG
  if (symb >= max_letters)
    throw bug_exception("tried to get symbolset of a non-symbol");
#endif
  return 1 << symb;
}

bool symbol::operator==(symbol const &s) const {
  return symb == s.symb;
}

bool symbol::operator!=(symbol const &s) const {
  return symb != s.symb;
}

symbol::operator char() {
#ifdef DEBUG
  if (symb == UNDEF) 
    throw bug_exception("action on undefined symbol");
#endif
  return alphabet[symb];
}

symbolset pickbit(symbolset &ss);

//////////////////////////////////////////////////////////////////////

void dumpset(symbolset ss);
void dumpsymbollist(symbol *s, int n);
std::ostream &operator <<(std::ostream &os, symbol *s);

int wordlen(symbol *st);

int numones(symbolset ss);

#endif
