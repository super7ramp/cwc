/**
 * cwc - a crossword compiler. Copyright 1999-2002 Lars Christensen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 **/

#include <iostream>

#include "symbol.hh"

//////////////////////////////////////////////////////////////////////
// class symbol

char symbol::alphabet[max_letters+5] = 
#if BIGALPHABET
  "                                "
#endif
  "                                "
  "# /+";

symbol symbol::alphindex[256];

unsigned char symbol::symballoc = 0;
symbol symbol::outside(max_letters + 1);
symbol symbol::none(max_letters + 2);
symbol symbol::empty(max_letters + 3);

symbol symbol::symbolbit(symbolset ss) {
  symbol s;
  for (symbolset i=1,n=0; i; i<<=1,n++) {
    if (ss&i)
      s.symb = n;
  }
  return s;      
}

symbol symbol::alloc(char ch) {
#ifdef DEBUG
  if (!isalpha(ch))
    throw bug_exception("tried to alloc a nonalpha letter");
#endif
  if (symballoc >= max_letters) {
    for (unsigned i=0; i<max_letters;i++) std::cout << alphabet[i]; std::cout << std::endl;
    std::cout << "Adding letter: " << ch << " (" << int((unsigned char)ch) << ")"<< std::endl;
    throw error("Too many letters in dictonary. Try compiling with -DBIGALPHABET.");
  }

  symbol s;
  s.symb = symballoc++;
  alphabet[s.symb] = ch;
  alphindex[(unsigned char)ch].symb = s.symb;

  return s;
}

void symbol::buildindex() {
  for (unsigned i=0;i<max_letters;i++)
    alphabet[i] = 0;
  for (int i=0;i<256;i++)
    alphindex[i] = symbol::none;
  symballoc = 0;
}

symbol::symbol(char ch) {
  *this = alphindex[(unsigned char)ch];
  if (*this == symbol::none)
    *this = symbol::alloc(ch);
}

symbolset pickbit(symbolset &ss) {
  symbolset a[max_letters], n = 0;
  for (symbolset i=1; i; i<<=1) {
    if (ss & i)
      a[n++] = i;
  }
  if (n==0) return 0;
  symbolset bit = a[rand()%n];
  ss &= ~bit;
  return bit;
}

int wordlen(symbol *st) {
  int n = 0;
  while (st[n] != symbol::outside) n++;
  return n;
}

std::ostream &operator <<(std::ostream &os, symbol *s) {
  while (*s != symbol::outside) {
    os << *s;
    s++;
  }
  return os;
}

int numones(symbolset ss) {
  int n = 0;
  for (symbolset i=1; i; i <<= 1) {
    if (ss&i)
      n++;
  }
  return n;
}

int symbol::numalpha() {
  int n = 0;
  for (unsigned i=0; i<max_letters; i++)
    if (isalpha(alphabet[i]))
      n++;
  return n;
}
