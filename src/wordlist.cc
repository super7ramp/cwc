/**
 * cwc - a crossword compiler. Copyright 1999-2002 Lars Christensen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 **/

#include <fstream>
#include <iostream>
#include "wordlist.hh"

using namespace std;

wordlist::wordlist() {
  allalpha = 0;
}

#define chunksize 8192

bool wordlist::wordok(const string &fn) {
  int n = fn.length();
  for (int i=0;i<n;i++)
    if (setup.all_words) {
      if (!isgraph(fn[i]))
	return false;
    } else {
      if (!isalpha(fn[i]))
	return false;
    }
  return true;
}

void wordlist::load(const string &fn) {
  ifstream f(fn.c_str());
  if (!f.is_open()) {
    cerr << "Failed to open dictionary file (" << fn << ")" << endl;
    cerr << "Use the -d swich to specify dictionary." << endl;
    throw error("Failed to open file");
  }

  widx.clear();

  symbol *chunk = new symbol[chunksize];
  int chunkused = 0;

  nwords = ignored_words = 0;

  string ln;
  while (!f.eof()) {
    getline(f, ln);
    int wlen = ln.length();
    if (wlen == 0)
      continue;
    if (!wordok(ln)) {
      ignored_words++;
      continue;
    }
    
    if (chunksize - chunkused < wlen+1) {
      chunk = new symbol[chunksize];
      chunkused = 0;
    }

    symbol *addr = chunk + chunkused;
    for (int i=0; i<wlen; i++) {
      symbol s = symbol((char)tolower(ln[i]));
      chunk[chunkused++] = s;
      allalpha |= s.getsymbolset();
    }
    chunk[chunkused++] = symbol::outside;

    widx.push_back(addr);
    nwords++;
  }

  if (setup.verboseness >= V_INFO)
    cout << "wordlist: " << nwords << " words loaded (" << ignored_words << " words ignored)" << endl;
  if (nwords < 1000) {
    cerr << "Warning: Your dictionary contains very few words. Filling a grid may not be possible" << endl;
  }
  if (nwords > 100000) {
    cerr << "Warning: Your dictionary is quite large. Filling a grid may take very long time." << endl;
  }
}
  
