/**
 * cwc - a crossword compiler. Copyright 1999-2002 Lars Christensen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA. 
 **/

#include <string>
#include <fstream>
#include <iostream>

using namespace std;

#include "symbol.hh"
#include "dict.hh"

//////////////////////////////////////////////////////////////////////
// class symbollink

int symbollink::instancecount = 0;

symbollink::symbollink() : symb(symbol::outside), target(0), next(0) {
  instancecount++;
}

symbollink *symbollink::addlink(symbol s) {
  symbollink *sl = new symbollink();
  sl->symb = s;
  sl->next = target;
  target = sl;
  return target;
}

void symbollink::addword(symbol *str, int n) {
  if (n == 0) return;
  if (!isalpha(str[0]))
    throw error("!!!");
  symbollink *sl = getlink(str[0]);
  if (sl == 0)
    sl = addlink(str[0]);
  sl->addword(str+1, n-1);
}

symbollink *symbollink::getlink(symbol s) {
  for (symbollink *sl = target; sl != 0; sl = sl->next) {
    if (sl->symb == s) return sl;
  }
  return 0;
}

bool symbollink::findpossible(symbol *s, int len,
				   int pos, symbolset &ss) {
  if ((target == 0)&&(len==0)) {
    if (pos == 0)
      ss |= symb.getsymbolset();
    return true;
  }
  if ((target==0)||(len==0))
    return false;

  if (s[0] == symbol::empty) {
    // search each subtree and OR the result.
    bool atallany = false;
    for (symbollink *sl = target; sl != 0; sl = sl->next) {
      bool any = sl->findpossible(s+1, len-1, pos-1, ss);
      if (any) {
	atallany = true;
        if (pos==0)
	  ss |= sl->symb.getsymbolset();
      }
    }
    return atallany;
  } else {
    // search specific subtree
    symbollink *sl = getlink(s[0]);
    if (sl == 0)
      return false;
    else {
      if (sl->findpossible(s+1, len-1, pos-1, ss)) {
	if (pos == 0)
	  ss |= sl->symb.getsymbolset();
	return true;
      }
      return false;
    }
  }
}

void symbollink::dump(char *prefix, int len) {
  if (target == 0) {
    cout << prefix << symb << endl;
    return;
  }

  if (prefix == 0) {
    prefix = new char[256];
    prefix[0] = '\0';
    for (symbollink *sl = target; sl; sl = sl->next) {
      sl->dump(prefix, len);
    }
    delete[] prefix;
  } else {
    prefix[len++] = symb; prefix[len] = '\0';
    for (symbollink *sl = target; sl; sl = sl->next) {
      sl->dump(prefix, len);
    }
    prefix[--len] = '\0';
  }
}
//////////////////////////////////////////////////////////////////////
// dict

dict::dict() {
}

dict::~dict() {
}
